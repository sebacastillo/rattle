# README #

Rattle - the R Analytical Tool To Learn Easily - is a popular GUI for data mining using R. It presents statistical and visual summaries of data, transforms data that can be readily modelled, builds both unsupervised and supervised models from the data, presents the performance of models graphically, and scores new datasets. One of the most important features (according to its author) is that all of your interactions through the graphical user interface are captured as an R script that can be readily executed in R independently of the Rattle interface.

Rattle is Free (as in Libre) Open Source Software and the source code is available here on Bitbucket. We give you the freedom to review the code, use it for whatever purpose you like, and to extend it however you like, without restriction, except that if you then distribute your changes you also need to distribute your source code too.

Whilst Rattle is available from [CRAN](https://cran.r-project.org/) you can install the latest developments using

`> devtools::install_bitbucket("kayontoga/rattle")`

Rattle is developed as an open source product by
[Togaware](http://www.togaware.com/) and is freely available to
anyone. Details are available from the
[Rattle](http://rattle.togaware.com/) home page.